import React, { Component } from 'react';
import axios from 'axios';
import {
  ActivityIndicator,
  AsyncStorage,
  Button,
  StatusBar,
  StyleSheet,
  View,
  Text,
  TextInput,
  BackHandler,
  TouchableOpacity,
  Image,
} from 'react-native'
import {
  createStackNavigator,
  createSwitchNavigator,
  createAppContainer,
} from 'react-navigation'
import { Calendar, CalendarList, Agenda, calendarTheme } from 'react-native-calendars';
import { ScrollView } from 'react-native-gesture-handler';
import { DocumentPicker, ImagePicker } from 'expo';


BackHandler.addEventListener('hardwareBackPress', function() {
  return false
})
class SignInScreen extends React.Component {
  static navigationOptions = {
    title: 'Органайзер для студента',

    headerStyle: {
      backgroundColor: "#3949ab",
      elevation: null,
    },
    headerTitleStyle: {
      color: '#ffffff'
    },
    headerTintColor: '#ffffff',
  }

  render() {
    return (
      <View style={styles.container}>
        <Image
          style={{ width: 250, height: 250 }}
          source={require('./assets/logo.jpg')}
        />
        <TouchableOpacity style={styles.button} onPress={this._routeToLogin}>
          <Text style={styles.textButton}>Войти</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.button} onPress={this._signUpAsync}>
          <Text style={styles.textButton}>Регистрация</Text>
        </TouchableOpacity>
      </View>
    )
  }

  _routeToLogin = async () => {
    this.props.navigation.navigate('Login')
  }
  _signUpAsync = () => {
    this.props.navigation.navigate('SignUp')
  }
}

class SignInForm extends React.Component {
  static navigationOptions = {
    title: 'Авторизация',
    headerStyle: {
      backgroundColor: "#3949ab",
      elevation: null
    },
    headerTitleStyle: {
      color: '#ffffff'
    },
  }
  constructor() {
    super()
    this.state = {
      email: '',
      password: '',
    }
  }

  handleChangeField = name => value => {
    this.setState({ [name]: value })
  }

  render() {
    const { email, password } = this.state
    return (
      <View style={styles.container}>
        <TextInput
          style={styles.textInput}
          placeholder="Email"
          value={email}
          onChangeText={this.handleChangeField('email')}
        />
        <TextInput
          style={styles.textInput}
          placeholder="Пароль"
          value={password}
          onChangeText={this.handleChangeField('password')}
        />
        <TouchableOpacity style={styles.button} onPress={this._signInAsync}>
          <Text style={styles.textButton}>Войти</Text>
        </TouchableOpacity>
      </View>
    )
  }
  _signInAsync = async () => {
    if (
      this.state.email != 'example@gmail.com' &&
      this.state.password != '123123'
    ) {
      alert('Введите корректные данные')
      return false
    }
    await AsyncStorage.setItem('userToken', 'abc')
    this.props.navigation.navigate('App')
  }
}

class Notes extends React.Component {
  static navigationOptions = {
    title: 'Заметки',
    headerStyle: {
      backgroundColor: "#3949ab",
      elevation: null
    },
    headerTitleStyle: {
      color: '#ffffff'
    },
  }
  constructor() {
    super()
    this.state = {
      notes: {
        id: 1,
        text: 'Нужно что-то запомнить...',
      },
    }
  }
  render() {
    const { notes } = this.state
    return (
      <ScrollView>
        <TouchableOpacity
        style={styles.toucheble}
          onPress={() => this.props.navigation.navigate('TextNote', { notes })}
        >
          <Text style={{ color: 'black' }}>{notes.text}</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.toucheble}
          onPress={() => this.props.navigation.navigate('TextNote', { notes })}
        >
          <Text style={{ color: 'black' }}>{notes.text}</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.toucheble}
          onPress={() => this.props.navigation.navigate('TextNote', { notes })}
        >
          <Text style={{ color: 'black' }}>{notes.text}</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.toucheble}
          onPress={() => this.props.navigation.navigate('TextNote', { notes })}
        >
          <Text style={{ color: 'black' }}>{notes.text}</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.absoluteBtn} onPress={() => this.props.navigation.navigate('TextNote')}>
          <Text style={styles.textButton}>Добавить</Text>
        </TouchableOpacity>
      </ScrollView>
    )
  }
}
class TextNote extends React.Component {
  static navigationOptions = {
    title: 'Редактирование',
    headerStyle: {
      backgroundColor: "#3949ab",
      elevation: null
    },
    headerTitleStyle: {
      color: '#ffffff'
    },
  }
  render() {
    const { navigation } = this.props
    const notes = navigation.getParam('notes', 'some default value')
    return (
      <View style={styles.container}>
        <TextInput value={notes.text} />
        <TouchableOpacity style={styles.button}>
          <Text style={styles.textButton}>Сохранить</Text>
        </TouchableOpacity>
      </View>
    )
  }
}
class TextNotes extends React.Component {
  static navigationOptions = {
    title: 'Задачи/Заметки',
    headerStyle: {
      backgroundColor: "#3949ab",
      elevation: null
    },
    headerTitleStyle: {
      color: '#ffffff'
    },
  }
  constructor() {
    super()
    this.state = {
      notes: {
        id: 1,
        text: 'lorem ipsum dolor sit amet',
      },
    }
  }
  render() {
    const { notes } = this.state
    return (
      <ScrollView>
        <TouchableOpacity
          style={styles.toucheble}
          onPress={() => this.props.navigation.navigate('Note', { notes })}
        >
          <Text style={{ color: 'black' }}>{notes.text}</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.toucheble}
          onPress={() => this.props.navigation.navigate('Note', { notes })}
        >
          <Text style={{ color: 'black' }}>{notes.text}</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.toucheble}
          onPress={() => this.props.navigation.navigate('Note', { notes })}
        >
          <Text style={{ color: 'black' }}>{notes.text}</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.toucheble}
          onPress={() => this.props.navigation.navigate('Note', { notes })}
        >
          <Text style={{ color: 'black' }}>{notes.text}</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.absoluteBtn} onPress={() => this.props.navigation.navigate('Note')}>
          <Text style={styles.textButton}>Добавить</Text>
        </TouchableOpacity>
      </ScrollView>
    )
  }
}
class Schedule extends Component {
  constructor(props) {
    super(props);
    this.state = {
      numerator: {
        lector: [
          'сп. Качанова Л.П.',
        ],
        name: 'ХИМИЯ',
        room: [
          '326'
        ],
        isPractice: true
      },
      items: {}
    }
  }
  static navigationOptions = {
    title: 'Мое расписание',
    headerStyle: {
      backgroundColor: "#3949ab",
      elevation: null
    },
    headerTitleStyle: {
      color: '#ffffff'
    },
  }

  render() {
    return (
      <Agenda
        items={this.state.items}
        loadItemsForMonth={this.loadItems.bind(this)}
        selected={'2019-05-16'}
        renderItem={this.renderItem.bind(this)}
        renderEmptyDate={this.renderEmptyDate.bind(this)}
        rowHasChanged={this.rowHasChanged.bind(this)}
        firstDay={1}
        //markingType={'period'}
      // markedDates={{
      //    '2019-05-08': {textColor: '#666'},
      //    '2019-05-09': {textColor: '#666'},
      //    '2019-05-14': {startingDay: true, endingDay: true, color: 'blue'},
      //    '2019-05-21': {startingDay: true, color: 'blue'},
      //    '2019-05-22': {endingDay: true, color: 'gray'},
      //    '2019-05-24': {startingDay: true, color: 'gray'},
      //    '2019-05-25': {color: 'gray'},
      //    '2019-05-26': {endingDay: true, color: 'gray'}}}
      // monthFormat={'yyyy'}
      // theme={{calendarBackground: 'red', agendaKnobColor: 'green'}}
      //renderDay={(day, item) => (<Text>{day ? day.day: 'item'}</Text>)}
      />
    );
  }

  loadItems(day) {
    setTimeout(() => {
      for (let i = -15; i < 85; i++) {
        const time = day.timestamp + i * 24 * 60 * 60 * 1000;
        const strTime = this.timeToString(time);
        if (!this.state.items[strTime]) {
          this.state.items[strTime] = [];
          const numItems = Math.floor(Math.random() * 5);
          for (let j = 0; j < numItems; j++) {
            this.state.items[strTime].push({
              name: 'Item for ' + strTime,
              height: Math.max(50, Math.floor(Math.random() * 150))
            });
          }
        }
      }
      //console.log(this.state.items);
      const newItems = {};
      Object.keys(this.state.items).forEach(key => { newItems[key] = this.state.items[key]; });
      this.setState({
        items: newItems
      });
    }, 1000);
    // console.log(`Load Items for ${day.year}-${day.month}`);
  }

  renderItem(item) {
    return (
      <View style={[styles.item, { height: 35 }]}><Text>{this.state.numerator.name}</Text></View>
    );
  }

  renderEmptyDate() {
    return (
      <View style={styles.emptyDate}><Text>Выходной</Text></View>
    );
  }

  rowHasChanged(r1, r2) {
    return r1.name !== r2.name;
  }

  timeToString(time) {
    const date = new Date(time);
    return date.toISOString().split('T')[0];
  }
}
class Note extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: {}
    };
  }
  static navigationOptions = {
    title: 'Редактирование',
    headerStyle: {
      backgroundColor: "#3949ab",
      elevation: null
    },
    headerTitleStyle: {
      color: '#ffffff'
    },
  }

  render() {
    return (
      <Agenda
        items={this.state.items}
        loadItemsForMonth={this.loadItems.bind(this)}
        selected={'2019-05-16'}
        renderItem={this.renderItem.bind(this)}
        renderEmptyDate={this.renderEmptyDate.bind(this)}
        rowHasChanged={this.rowHasChanged.bind(this)}
      // markingType={'period'}
      // markedDates={{
      //    '2017-05-08': {textColor: '#666'},
      //    '2017-05-09': {textColor: '#666'},
      //    '2017-05-14': {startingDay: true, endingDay: true, color: 'blue'},
      //    '2017-05-21': {startingDay: true, color: 'blue'},
      //    '2017-05-22': {endingDay: true, color: 'gray'},
      //    '2017-05-24': {startingDay: true, color: 'gray'},
      //    '2017-05-25': {color: 'gray'},
      //    '2017-05-26': {endingDay: true, color: 'gray'}}}
      // monthFormat={'yyyy'}
      // theme={{calendarBackground: 'red', agendaKnobColor: 'green'}}
      //renderDay={(day, item) => (<Text>{day ? day.day: 'item'}</Text>)}
      />
    );
  }

  loadItems(day) {
    setTimeout(() => {
      for (let i = -15; i < 85; i++) {
        const time = day.timestamp + i * 24 * 60 * 60 * 1000;
        const strTime = this.timeToString(time);
        if (!this.state.items[strTime]) {
          this.state.items[strTime] = [];
          const numItems = Math.floor(Math.random() * 5);
          for (let j = 0; j < numItems; j++) {
            this.state.items[strTime].push({
              name: 'Item for ' + strTime,
              height: Math.max(50, Math.floor(Math.random() * 150))
            });
          }
        }
      }
      //console.log(this.state.items);
      const newItems = {};
      Object.keys(this.state.items).forEach(key => { newItems[key] = this.state.items[key]; });
      this.setState({
        items: newItems
      });
    }, 1000);
    // console.log(`Load Items for ${day.year}-${day.month}`);
  }

  renderItem(item) {
    return (
      <View style={[styles.item, { height: item.height }]}><Text>{item.name}</Text></View>
    );
  }

  renderEmptyDate() {
    return (
      <View style={styles.emptyDate}><Text>This is empty date!</Text></View>
    );
  }

  rowHasChanged(r1, r2) {
    return r1.name !== r2.name;
  }

  timeToString(time) {
    const date = new Date(time);
    return date.toISOString().split('T')[0];
  }
}

class FileManager extends React.Component {
  static navigationOptions = {
    title: 'Менеджер файлов',
    headerStyle: {
      backgroundColor: "#3949ab",
      elevation: null
    },
    headerTitleStyle: {
      color: '#ffffff'
    },
  }
  render() {
    return (
      <ScrollView>
        <Text>eqqeq</Text>
      </ScrollView>
    )
  }
}

class PassManager extends React.Component {
  static navigationOptions = {
    title: 'Менеджер Паролей',
    headerStyle: {
      backgroundColor: "#3949ab",
      elevation: null
    },
    headerTitleStyle: {
      color: '#ffffff'
    },
  }
  constructor() {
    super()
    this.state = {
      password: {
        id: 1,
        resourse: 'www.example.com',
        login: 'dasha',
        password: '2131',
      },
    }
  }
  render() {
    const { password } = this.state
    return (
      <ScrollView>
        <TouchableOpacity
          style={styles.toucheble}
          onPress={() =>
            this.props.navigation.navigate('Password', { password })
          }
        >
          <Text style={{ color: 'black' }}>{password.resourse}</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.toucheble}
          onPress={() =>
            this.props.navigation.navigate('Password', { password })
          }
        >
          <Text style={{ color: 'black' }}>{password.resourse}</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.toucheble}
          onPress={() =>
            this.props.navigation.navigate('Password', { password })
          }
        >
          <Text style={{color: 'black'}}>{password.resourse}</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.toucheble}
          onPress={() =>
            this.props.navigation.navigate('Password', { password })
          }
        >
          <Text style={{ color: 'black' }}>{password.resourse}</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.absoluteBtn} onPress={() => this.props.navigation.navigate('Password')}>
          <Text style={styles.textButton}>Добавить</Text>
        </TouchableOpacity>
      </ScrollView>
    )
  }
}

class Password extends React.Component {
  static navigationOptions = {
    title: 'Редактирование',
    headerStyle: {
      backgroundColor: "#3949ab",
      elevation: null
    },
    headerTitleStyle: {
      color: '#ffffff'
    },
  }
  render() {
    const { navigation } = this.props
    const password = navigation.getParam('password', 'some default value')
    return (
      <View style={styles.container}>
        <Text>Ресурс</Text>
        <TextInput style={styles.textInput} value={password.resourse} />
        <Text>Логин</Text>
        <TextInput style={styles.textInput} value={password.login} />
        <Text>Пароль</Text>
        <TextInput style={styles.textInput} value={password.password} />
        <TouchableOpacity style={styles.button}>
          <Text style={styles.textButton}>Сохранить</Text>
        </TouchableOpacity>
      </View>
    )
  }
}

class HomeScreen extends React.Component {
  static navigationOptions = {
    title: 'Органайзер для студента',
    headerStyle: {
      backgroundColor: "#3949ab",
      elevation: null
    },
    headerTitleStyle: {
      color: '#ffffff'
    },
  }

  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity style={styles.button} onPress={() => this._showMoreApp('Schedule')}>
          <Text style={styles.textButton}>Расписание</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.button} onPress={() => this._showMoreApp('Notes')}>
          <Text style={styles.textButton}>Задачи</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.button} onPress={() => this._showMoreApp('Notes')}>
          <Text style={styles.textButton}>Заметки</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.button} onPress={() => this._showMoreApp('PassManager')}>
          <Text style={styles.textButton}>Менеджер паролей</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.button} onPress={() => this._showMoreApp('Docs')}>
          <Text style={styles.textButton}>Менеджер файлов</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.button} onPress={this._signOutAsync}>
          <Text style={styles.textButton}>Выход</Text>
        </TouchableOpacity>
      </View>
    )
  }

  _showMoreApp = route => {
    this.props.navigation.navigate(route)
  }

  _signOutAsync = async () => {
    await AsyncStorage.clear()
    this.props.navigation.navigate('Auth')
  }
}

class Register extends React.Component {
  static navigationOptions = {
    title: 'Регистрация',
    headerStyle: {
      backgroundColor: "#3949ab",
      elevation: null
    },
    headerTitleStyle: {
      color: '#ffffff'
    },
  }
  constructor() {
    super()
    this.state = {
      name: '',
      sname: '',
      birthday: '',
      faculty: '',
      curse: new Number,
      group: new Number,
      email: '',
      password: '',
      spassword: ''
    }
  }

  render() {
    const {name, sname, birthday, faculty, group, email, course, password, spassword } = this.state;
    return (
      <View style={styles.container}>
        <Text>Имя</Text>
        <TextInput style={styles.textInput} value={name}
          onChangeText={this.handleChangeField('name')}/>
        <Text>Фамилия</Text>
        <TextInput style={styles.textInput} value={sname}
          onChangeText={this.handleChangeField('sname')}/>
        <Text>Дата рождения</Text>
        <TextInput style={styles.textInput} value={birthday}
          onChangeText={this.handleChangeField('birthday')}/>
        <Text>Факультет</Text>
        <TextInput style={styles.textInput} value={faculty}
          onChangeText={this.handleChangeField('faculty')}/>
        <Text>Курс</Text>
        <TextInput style={styles.textInput} value={course}
          onChangeText={this.handleChangeField('course')}/>
        <Text>Группа</Text>
        <TextInput style={styles.textInput} value={group}
          onChangeText={this.handleChangeField('group')}/>
        <Text>Email</Text>
        <TextInput style={styles.textInput} value={email}
          onChangeText={this.handleChangeField('email')}/>
        <Text>Пароль</Text>
        <TextInput style={styles.textInput} value={password}
          onChangeText={this.handleChangeField('password')}/>
        <Text>Повторите пароль</Text>
        <TextInput style={styles.textInput} value={spassword}
          onChangeText={this.handleChangeField('spassword')}/>
        <TouchableOpacity style={styles.button} onPress={this.register}>
          <Text style={styles.textButton}>Создать аккаунт</Text>
        </TouchableOpacity>
      </View>
    )
  }
  handleChangeField = name => value => {
    this.setState({ [name]: value })
  }
  register = async () => {
    if(this.state.password != this.state.spassword) {
      alert('Пароли не совпадают')
    } else {
      const data = {};
      data.name = this.state.name;
      data.sname = this.state.sname;
      data.birthday = this.state.birthday;
      data.faculty = this.state.faculty;
      data.course = this.state.course;
      data.group = this.state.group;
      data.email = this.state.email;
      data.password = this.state.password;
      axios.post('Здесь путь к роуту', data).then((response) => {
        if(response.status < 300) {
          alert('Регистрация прошла успешно')
        } else {
          alert('Что-то пошло не так')
        }
      }).catch((e) => {alert(e)})
    }
  }
}
class OtherScreen extends React.Component {
  static navigationOptions = {
    title: 'Lots of features here',
  }

  render() {
    return (
      <View style={styles.container}>
        <Button title="I'm done, sign me out" onPress={this._signOutAsync} />
        <StatusBar barStyle="default" />
      </View>
    )
  }

  _signOutAsync = async () => {
    await AsyncStorage.clear()
    this.props.navigation.navigate('Auth')
  }
}

class AuthLoadingScreen extends React.Component {
  constructor() {
    super()
    this._bootstrapAsync()
  }

  _bootstrapAsync = async () => {
    const userToken = await AsyncStorage.getItem('userToken')
    this.props.navigation.navigate(userToken ? 'App' : 'Auth')
  }

  render() {
    return (
      <View style={styles.container}>
        <ActivityIndicator />
        <StatusBar barStyle="default" />
      </View>
    )
  }
}

class Document extends React.Component {
  static navigationOptions = {
    title: 'Менеджер файлов',
    headerStyle: {
      backgroundColor: "#3949ab",
      elevation: null
    },
    headerTitleStyle: {
      color: '#ffffff'
    },
  }
  constructor() {
    super()
    this.state = {
      fileUri: '',
      fileType: '',
      fileName: '',
      fileSize: '',
      image: ''
    }
  }

  handleChangeField = name => value => {
    this.setState({ [name]: value })
  }
  _pickDocument = async () => {
    let result = await DocumentPicker.getDocumentAsync({});
    if (!result.cancelled) {
      this.setState({ fileUri: result.uri });
      this.setState({ fileType: result.type });
      this.setState({ fileName: result.name });
      this.setState({ fileSize: result.size });
    }
  }

  _pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      allowsEditing: true,
      aspect: [4, 3],
    });

    alert(result.uri);
    console.log(result)

    if (!result.cancelled) {
      this.setState({ image: result.uri });
    }
  };

  render() {
    let { fileType, fileUri, fileSize, fileName } = this.state;
    return (
      <View style={styles.container}>
        <TouchableOpacity
          activeOpacity={0.5}
          style={{ alignItems: 'center' }}
          onPress={this._pickDocument}>
          <Image
            source={{
              uri:
                'https://aboutreact.com/wp-content/uploads/2018/09/clips.png',
            }}
            style={styles.ImageIconStyle}
          />
          <Text style={{ marginTop: 10 }}>Add Attachment</Text>
        </TouchableOpacity>
        <Text style={styles.text}>
          {fileUri ? 'URI\n' + fileUri : ''}
        </Text>
        <Text style={styles.text}>
          {fileType ? 'Type\n' + fileType : ''}
        </Text>
        <Text style={styles.text}>
          {fileName ? 'File Name\n' + fileName : ''}
        </Text>
        <Text style={styles.text}>
          {fileSize ? 'File Size\n' + fileSize : ''}
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#e0e0e0'
  },
  textInput: {
    width: 300,
    borderColor: '#3949ab',
    borderRadius: 25,
    borderWidth: 1,
    paddingHorizontal: 16,
    fontSize: 16,
    marginTop: 10
  },
  textButton: {
    color: '#ffffff',
    fontSize: 16,
    margin: 5
  },
  textArea: {
    height: 350,
  },
  button: {
    width: 250,
    height: 40,
    borderRadius: 25,
    marginTop: 10,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: "#3949ab",
  },
  absoluteBtn: {
    width: 150,
    height: 30,
    alignSelf: 'flex-end',
    marginTop: 20,
    borderBottomLeftRadius: 25,
    borderTopLeftRadius: 25,
    backgroundColor: "#3949ab",
  },
  toucheble: {
    backgroundColor: 'white',
    borderBottomColor: 'black',
    borderBottomWidth: 1,
    padding: 10,
    
  },
  item: {
    backgroundColor: 'white',
    flex: 1,
    borderRadius: 5,
    padding: 10,
    marginRight: 10,
    marginTop: 17
  },
  emptyDate: {
    height: 15,
    flex: 1,
    paddingTop: 30
  },
  text: {
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: 15,
    textAlign: 'center',
    marginTop: 16,
    color: 'black',
  },
  ImageIconStyle: {
    height: 80,
    width: 80,
    resizeMode: 'stretch',
  },
})

const AppStack = createStackNavigator({
  Home: HomeScreen,
  Other: OtherScreen,
  TextNotes: TextNotes,
  PassManager: PassManager,
  FileManager: FileManager,
  Note: Note,
  Password: Password,
  Schedule: Schedule,
  Notes: Notes,
  TextNote: TextNote,
  Docs: Document
})
const AuthStack = createStackNavigator({
  SignIn: SignInScreen,
  SignUp: Register,
  Login: SignInForm,
})

export default createAppContainer(
  createSwitchNavigator(
    {
      AuthLoading: AuthLoadingScreen,
      App: AppStack,
      Auth: AuthStack,
    },
    {
      initialRouteName: 'AuthLoading',
    },
  ),
)
